import * as express from "express";
import * as mongoose from "mongoose";
import SurveyModel from "../models/survey"
import { getEnvironmentVariables } from "./envoirments/env";
// import body-parser from "body-parser";

const expressGraphQL = require("express-graphql");
var PORT = process.env.PORT || 4000;

const MainSchema = require("./graphql/schema");
var app: express.Application = express();

var port: Number = parseInt(<string>process.env.PORT) || 9000;

mongoose
  .connect(getEnvironmentVariables().db_url)
  .then(() => {
    app.listen(port, () => {
      console.log("running with ", getEnvironmentVariables().db_url);
    });
  })
  .catch(err => {
    console.log(err);
  });



// app.use('/createuser',(req,res)=>{
//   var newUser = new usersModel({
//     name: args.name,
//     age: args.age,
//     phoneNumber: args.phoneNumber,
//     status: args.status,
//     email: args.email,
//     token: args.token
//   });
//   try {
//     let response = await newUser.save();
//     return response;
//   } catch (error) {
//     return error;
//   }
// })

app.use(
  "/graphql",
  expressGraphQL((request, response) => {
    return {
      graphiql: true,
      schema: MainSchema
    };
  })
);
