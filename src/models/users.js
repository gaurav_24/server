const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const usersModel = new Schema(
  {
    name: String,
    age: Number,
    email: String,
    status: Boolean,
    phoneNumber: Number,
    password: String
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("users", usersModel);
