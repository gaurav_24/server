const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const inforUser = new Schema(
  {
    name: String,
    age: String,
    email: String,
    status: String,
    phoneNumber: String,
    password: String
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("infousers", inforUser);
