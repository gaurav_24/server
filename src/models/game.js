const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const gameModel = new Schema(
  {
    gameName: String,
    phoneNumber: String,
    userScore: Number, 
    email: String,
    name: String

  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("games", gameModel);
