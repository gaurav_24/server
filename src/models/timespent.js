const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const timeModel = new Schema(
  {
    username: String,
    pageid: String,
    time: String,
    useremail: String,
   
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("timespends", timeModel);
